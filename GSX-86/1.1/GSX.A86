;
;	GSX-86 1.1 resident portion
;
;	Backported from the 19 December 1983 GSX-86 1.3 source code release
;
GSX_Code	CSeg	para	Public

		Public	GSX_Entry_0
		Public	GSX_Entry_1
		Public	GSX_Entry
		Public	Old_Vector_0
		Public	Old_Vector_1
		Public	UnBound
		Public	BoundIndirect
		Public	BoundFcb
		Public	BoundDirect
		Public	Default_Drive
		Public	Open_Assign
		Public	Read_Assign
		Public	Close_File
		Public	Open_Driver
		Public	GSX_Bdos
		Public	GSX_Bytes
		Public	GSX_Comm_Seg
		Public	GSX_Min_Stack

CPM_Version	equ	0001
CPMPLUS_Version equ	0002
MPM_Version	equ	0003
CCPM_Version	equ	0004

SetupRSX	equ	192	; RSX subfunction code to intercept GDOS calls
RSXTermret	equ	193	; RSX subfunction number to release GSX at
RSXTermdie	equ	194	; segment and offset specified in RSXPCB block
				; and release the RSX

; Values for HowBound

UnBound		Equ	-1		;  Nothing bound to this layer
BoundIndirect	Equ	0		;  Proc Bound Indirect
BoundFcb	Equ	1		;  Proc Bound by Fcb
BoundDirect	Equ	2		;  Proc Bound Direct


Minimum_Stack	Equ	20H
Stack_Height	Equ	200H

Bdos	Equ	224			; Int code for Bdos
Gdos	Equ	Bdos			; Int code for Gdos
Gdos_Int	Equ	Bdos		; Int value for Gdos
Gdos_Function	Equ	115		; Cl value for Gdos

Gdos_V		Equ	Gdos_Int*4

Gdos_Vector	Equ	DWord Ptr .Gdos_V	; Gdos interrupt vector

ErrorCode1	Equ	1
ErrorCode2	Equ	2
ErrorCode3	Equ	3
ErrorCode4	Equ	4
ErrorCode5	Equ	5
ErrorCode5a	Equ	5
ErrorCode6	Equ	6
ErrorCode7	Equ	7
ErrorCode8	Equ	8
ErrorCode9	Equ	9
ErrorCode10	Equ	10
ErrorCode11	Equ	11
ErrorCode12	Equ	12
ErrorCode13	Equ	13
ErrorCode14	Equ	14
ErrorCode15	Equ	15
ErrorCode16	Equ	16
ErrorCode17	Equ	17
ErrorCode18	Equ	18
ErrorCode19	Equ	19
ErrorCode20	Equ	20
ErrorCode21	Equ	21
ErrorCode22	Equ	22
Hacksize	equ	4


GSX_Entry_0:			; Int 224 entry
	Cmp	Cl,115		; Gdos function code?
	Je	GSX_Entry	; Jump if yes

;	Jmpf	Far Ptr Old_Vector_0
	Db	0eah		; Jmpf opcode

Old_Vector_0	Rd	1

GSX_Entry_1:			; Int 225 entry
	Cmp	Cl,115		; Gdos function code?
	Je	GSX_Entry	; Jump if yes

;	Jmpf	Far Ptr Old_Vector_1
	Db	0eah		; Jmpf opcode

Old_Vector_1	Rd	1


GSX_Entry:
	Sub	Sp,4			; Room for Far Ptr to layer
	Push	Bp			; Save caller's Bp
	Mov	Bp,Sp			; Need access into stack

; Right now:	[Bp]		Caller's Bp
;		6[Bp]		Caller's Ip
;		8[Bp]		Caller's Cs
;		10[Bp]		Caller's Flags

	Push	Word Ptr 10[Bp]		; Push caller's flags
	Popf				; Restore interrupt state

; After futzing:
OldCs		Equ	Ss:(Word Ptr 10)[Bp]; Caller's Cs
OldIp		Equ	Ss:(Word Ptr 8)[Bp] ; Caller's Ip
ProcCs		Equ	Ss:(Word Ptr 6)[Bp] ; Layer Cs if PassThru or RetPtr
ProcIp		Equ	Ss:(Word Ptr 4)[Bp] ; Layer Ip if PassThru or RetPtr
OldPtsInSegment Equ	Ss:(Word Ptr 6)[Bp] ; Caller's original Dword Ptr to
OldPtsInOffset	Equ	Ss:(Word Ptr 4)[Bp] ;	PtsIn if InvokeDriver
OldFlags	Equ	Ss:(Word Ptr 2)[Bp] ; Caller's flags
OldBp		Equ	Ss:(Word Ptr 0)[Bp] ; Caller's Bp
OldAx		Equ	Ss:(Word Ptr (-2))[Bp]; Caller's Ax
OldBx		Equ	Ss:(Word Ptr (-4))[Bp]; Caller's Bx
OldCx		Equ	Ss:(Word Ptr (-6))[Bp]; Caller's Cx
OldDx		Equ	Ss:(Word Ptr (-8))[Bp]; Caller's Dx
OldSi		Equ	Ss:(Word Ptr (-10))[Bp]; Caller's Si
OldDi		Equ	Ss:(Word Ptr (-12))[Bp]; Caller's Di
OldDs		Equ	Ss:(Word Ptr (-14))[Bp]; Caller's Ds
OldEs		Equ	Ss:(Word Ptr (-16))[Bp];Caller's Es
LayerIndex	Equ	Ss:(Word Ptr (-18))[Bp]; Pointer into Layer table

	Push	Ax			; Preserve caller's registers
	Push	Bx
	Push	Cx
	Push	Dx
	Push	Si
	Push	Di
	Push	Ds
	Push	Es


	Mov	Bx,Ss:(Word Ptr 10)[Bp]	; Get caller's flags
	Mov	OldFlags,Bx		;  to where they belong
	Mov	Bx,Ss:(Word Ptr 8)[Bp]	; Get caller's Cs
	Mov	OldCs,Bx		;  to where it belongs
	Mov	Bx,Ss:(Word Ptr 6)[Bp]	; Get caller's Ip
	Mov	OldIp,Bx		;  to where it belongs


	Mov	Bl,Ch			; Layer number *16 to Bl
	Mov	Bh,0			; High byte 0
	Mov	Si,Bx			; Layer & function to Si
	And	Bl,0f0h			; Mask to just layer number
	Cmp	Bx,MAXPROC_Paragraphs	; Illegal layer?
	Ja	Illegal			; Jump if yes
	Push	Bx			; To Tos

	And	Si,00fh			; Mask to just function code
	Cmp	Si,MAXFUNC		; Illegal function?
	Ja	Illegal			; Jump if yes
	Shl	Si,1			; Times 2 for function code table index
	Mov	Di,Dx			; Put param block offset into Di
	Jmp	Cs:Function[Si]		; Dispatch to specialists

; Ds:Di points to parameter block, if any
; Bx contains layer index

Function	Dw	Offset KillLayer	; Function 0
KillLayerOp	Equ	0
		Dw	Offset BindIndirect	;	   1
BindIndirectOp	Equ	1
		Dw	Offset BindFcb		;	   2
BindFcbOp	Equ	2
		Dw	Offset BindDirect	;	   3
BindDirectOp	Equ	3
		Dw	Offset InvokeDriver	;	   4
InvokeDriverOp	Equ	4
		Dw	Offset PassThrough	;	   5
PassThroughOp	Equ	5
		Dw	Offset ReturnPtr	;	   6
ReturnPtrOp	Equ	6
		Dw	Offset ReturnMaxProc	;	   7
ReturnMaxProcOp Equ	7

Illegal:	Jmp	Error2

Default_Drive	db	0	; ASSIGN.SYS drive
		db	'ASSIGN  SYS'


; Di and Dx contain the Procedure Id
; Bx contains the layer index
BindIndirect:
	Call	PushDmaAddress		; Save Dma Segment & Offset
	Mov	Al,0			; Assume proc gets loaded
	Cmp	Cs:HowBound[Bx],BoundIndirect; Current resident Bound Indirect?
	Jne	Search			; If not Bound Indirect must search
	Cmp	Di,Cs:ProcId[Bx]	; Asking for same proc again?
	Je	Quit			; Jump if already there and alive

Search:	
	Sub	Sp, 0CAh
	Push	Cs
	Call	Open_Assign
	Jnc	Assign_Loop		; If Carry is set, AH holds error;
	Mov	Ah, 0			; return it. Otherwise set AH to 0.
ErrorRoutine3:
	Mov	Al, 0FFh		; Requested driver not found.
					; [BUG] ASSIGN.SYS not closed?
Quit:
	Jmp	PopDmaAddress		; Restore caller's Dma & return w err
;
Assign_Loop:
	Push	Cs
	Call	Read_Assign	; Read and parse a line from ASSIGN.SYS
	Jnz	ErrorRoutine3	; End of file and requested driver not found
	Cmp	Dx, OldDx	; Is this the requested driver?
	Jnz	Assign_Loop	; Read next line
	Push	Ss
	Push	Bx
	Push	Cs
	Call	Close_File	; Close ASSIGN.SYS
	Mov	Ah, ErrorCode3
	Jc	ErrorRoutine3	; Error on close
	Mov	Sp, Di
	Push	Ss
	Pop	Ds
	Mov	Dx, Di
	Mov	Cx,OldCx		; Get caller's function code
	And	Ch,0f0h			; Preserve layer number
	Add	Ch,BindFcbOp		;  & change function to Bind by Fcb
	Int	Gdos			; Bind the program by Fcb
	Jnz	Quit			; Quit if error in load
	Mov	Bx, LayerIndex
	Mov	Cs:(Byte Ptr HowBound)[Bx],BoundIndirect; Flag how bound

	Mov	Dx,OldDx		; Get ProcId
	Mov	Cs:(Word Ptr ProcId)[Bx],Dx;  into layer table
	Jmps	Quit			;  & return with Al from BindFcbOp

BindFcb:
	Call	PushDmaAddress		; Save caller's Dma Segment & Offset
	Sub	Sp, 82h
	Mov	byte ptr -147[Bp], 1Ah	; Buffer
	Push	Ds
	Push	Dx			; FCB
	Push	Ss
	Lea	Si, -148[Bp]
	Push	Si			; Buffer
	Push	Cs
	Call	Open_Driver		; Open the driver and read its header
	jnz	BindFcbErr
	Mov	Si,LayerIndex		; point SI to the proper Layer
	Mov	Al,ErrorCode7
	Cmp	Bx,Cs:ModuleSize[Si]	; Will it fit in space available?
	Ja	BindFcbErr
	Mov	Cs:HowBound[si], Unbound
	Push	Ds
	Push	Dx			; FCB
	Push	Ss
	Lea	Bx, -148[Bp]		; Buffer
	Push	Bx
	Push	Cs			; Entry in layer table
	Push	Si
	Call	Load_Driver
	Jnz	BindFcbErr	
	Mov	Cs:HowBound[Si],BoundFcb; Flag layer as bound by Fcb
BFQuit:
	Jmp	PopDmaAddress		; Return, restoring Dma	
;
BindFcbErr:
	Mov	Ah, Al
	Mov	Al, 0FFh
	Jmps	BFQuit


BindDirect:
	Mov	Cs:(Word Ptr ProcPtr+2)[Bx],Ds	; Point to procedure
	Mov	Cs:(Word Ptr ProcPtr)[Bx],Dx	;  as Far pointer

	Mov	Cs:(Byte Ptr HowBound)[Bx],BoundDirect	; Flag as Bound Direct

	Jmp	GSXQuit			; Return to Gdos caller

InvokeDriver:

; Ds:Di points to parameter block.  Bx holds Layer Index.

	Les	Si,Ds:(Dword Ptr 8)[Di]	; Es:Si <- Ptr to PtsIn
	Mov	OldPtsInSegment,Es	; Save for after driver invocation
	Mov	OldPtsInOffset,Si

	Call	TransformToRaster	; Transform any PtsIn to raster space

	Les	Si,Ds:(Dword Ptr 0)[Di]	; Get pointer to Contrl array

	Cmp	Es:(Word Ptr 0)[Si],1	; Open WorkStation?
	Jne	IDNotOpenWS		; Jump if not Open WorkStation

; Open Workstation driver invocation

; Ds:Di point to parameter block
; Es:Si point to Contrl array

	Les	Si,Ds:(Dword Ptr 4)[Di]	; Get pointer to IntIn
	Mov	Si,Es:(Word Ptr 0)[Si]	; Get IntIn(1), Device Id

	Cmp	Cs:HowBound[Bx],UnBound	; Layer holds proc?
	Je	IDLoad			; Jump if no proc in layer

	Cmp	Si,Cs:ProcId[Bx]	; Device already loaded?
	Je	IDLoaded		; Jump if device driver already in

; Load the driver -- Device Id in Si
IDLoad:
	Mov	Dx,Si			; Device Id to Dx
	And	Ch,0f0h			; Mask to just layer number
	Add	Ch,BindIndirectOp	; Bind Indirect opcode
	Int	Gdos			; Try to load in the new driver

	Mov	Byte Ptr OldAx,Al	; Flag to caller if ok
	Jz	IDLoaded		; Jump if ok

	Mov	OldAx,Ax		; Flag bad load if bad load
	Cmp	Al, 8
	Jnz	IDLoaded
	Jmp	Error7

IDLoaded:
	Cmp	Cs:HowBound[Bx],UnBound	; Driver present?
	Jz	IDNotLoaded

	Push	OldFlags		; Get caller's original flags
	Popf				; Restore them
	Or	Al,Al			; Set status on Al value
	Pushf				; Save flags
	Pop	OldFlags		;  back for caller

	Call	Driver			; Invoke the driver

	Les	Si,Ds:(Dword Ptr 12)[Di]; Es:Si <- ptr to IntOut
	Mov	Ax,Es:(Word Ptr 0)[Si]	; Get highest X pixel address
	Inc	Ax			; Now number of pixels in X
	Mov	Cs:XPixels[Bx],Ax	; Save number of pixels in X

	Mov	Ax,Es:(Word Ptr 2)[Si]	; Get highest Y pixel address
	Inc	Ax			; Now number of pixels in Y
	Mov	Cs:YPixels[Bx],Ax	; Save number of pixels in Y

	Jmps	IDTransformOutPts	;  & go transform any output points

IDNotOpenWS:
	Cmp	Cs:HowBound[Bx],UnBound	; Driver present?

IDNotLoaded:
	Jz	Error4
	Call	Driver			; Invoke the driver

IDTransformOutPts:
	Lea	Sp,LayerIndex
	Les	Si,Dword Ptr OldPtsInOffset	; Restore caller's PtsIn ptr
	Mov	Ds:(Word Ptr 8)[Di],Si
	Mov	Ds:(Word Ptr 10)[Di],Es

	Call	TransformTo32K		; Transform any PtsOut to 32K space

GSXQuit:
	Lea	Sp,OldEs		; Trash LayerIndex

	Pop	Es			; Restore registers
	Pop	Ds
	Pop	Di
	Pop	Si
	Pop	Dx
	Pop	Cx
	Pop	Bx
	Pop	Ax

	Pop	OldPtsInSegment		; Orig Bp to just below return address
	Popf				; Restore driver's flags
	Pop	Bp			; Trash old PtsIn offset
	Pop	Bp			; Restore driver's Bp
	Retf				; Return to caller

PopDmaAddress:
	Push	Ax			; Save return code
	Mov	Dx,ProcCs		; Get caller's Dma Segment Base
	Mov	Cl,033h			; Bdos "Set Dma Segment Base" function
	Push	Cs
	Call	GSX_Bdos
	Mov	Dx,ProcIp		; Get caller's Dma Offset
	Mov	Cl,01ah			; Bdos "Set Dma Offset" function
	Push	Cs
	Call	GSX_Bdos
	Pop	Ax			; Restore return code

GSXQuitStuffAx:
	Mov	OldAx,Ax		; Return Al and maybe Ah to caller

	Push	OldFlags		; Get caller's flags back
	Popf

	Or	Al,Al			; Set caller's flags

	Pushf
	Pop	OldFlags

	Jmps	GSXQuit			;  & return to caller

KillLayer:
	Mov	Cs:(Byte Ptr HowBound)[Bx],UnBound	; Make layer unbound
	Jmps	GSXQuit			; & return to caller

Error4:
	Mov	Dx, Offset ErrorMesg1
	Call	ShowCsIpString
	Jmps	Abort

Error2:
	Mov	Dx, Offset ErrorMesg2
	Call	ShowCsIpString
	Mov	Dx, OldCx
	Call	PoopDx
Abort:
	Mov	Cl,0
	Sub	Dx,Dx
	Int	Bdos
;
Error7:	Mov	Dx,Offset ErrorMesg7
	Call	ShowCsIpString
	Mov	Dx,Si
	Call	PoopDx
	Mov	Dx,Offset HexString
	Call	ShowCsIpString
	Jmps	Abort

PassThrough:
	Cmp	Cs:(Byte Ptr HowBound)[Bx],UnBound	; Layer empty?
	Je	Error2

	Mov	Si,Cs:(Word Ptr ProcPtr+2)[Bx]	; Get layer Cs
	Mov	ProcCs,Si			;  to place for Retf
	Mov	Si,Cs:(Word Ptr ProcPtr)[Bx]	; Get layer Ip
	Mov	ProcIp,Si			;  to place for Retf

	Pop	Bx			; Trash LayerIndex

	Pop	Es			; Restore caller's registers
	Pop	Ds
	Pop	Di
	Pop	Si
	Pop	Dx
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Pop	Bp
	Popf				; Restore caller's flags

; Invoke the layer's procedure with a Retf so that it will appear as if
; the caller had done a Callf of the layer.
	Retf				; Invoke proc via Retf


ReturnPtr:
	Mov	Al,Cs:(Byte Ptr HowBound)[Bx]	; Return how proc was bound

	Push	Cs			; Base of Layer Table
	Pop	OldEs			;  to caller's Es

	Lea	Bx,(Byte Ptr HowBound)[Bx]; Bx <- offset of entry
	Mov	OldBx,Bx		;  to caller's Bx

	Jmp	GSXQuitStuffAx		; Quit, returning proc's Cs:Ip in Es:Bx

ReturnMaxProc:
	Mov	Ax,MAXPROC		; Return maximum layer number
	Jmp	GSXQuit			;  & return to caller
					; [1.3 uses GSXQuitStuffAx here]
;
; Open ASSIGN.SYS
;
Open_Assign:
	Mov	Si, Offset Default_Drive
	Mov	Cx, 6
	Lea	Di, -90[Bp]
	Push	Ss
	Pop	Es
	Cld
Copy_Fcb:
	Movs	Es:word ptr [Di], Cs:word ptr [Si]
	Loop	Copy_Fcb
	Mov	byte ptr -219[Bp], 1Ah
	Lea	Bx, -90[Bp]
	Push	Ss
	Push	Bx
	Lea	Si, -220[Bp]
	Push	Ss
	Push	Si
	Call	Open_FCB
	Lea	Di, -54[Bp]
	Mov	Al, 20h
	Retf
;
Read_Assign:
	Push	Ax
	Mov	Al,Cs:Default_Drive
	Mov	-54[Bp], Al
	Mov	Al, 'S'
	Mov	-45[Bp], Al
	Mov	Ax, 'SY'
	Mov	-44[Bp], Ax	;BP-54 holds FCB: X:________.SYS
	Pop	Ax
	Mov	Ah, 2
Parse_Line:
	Push	Ss
	Push	Bx
	Push	Ss
	Push	Si
	Call	Look_For_Number
	Jnc	Number_Found
NoNumFound:
	Cmp	Al, 1Ah		; EOF?
	Jz	Read_Assign_End
	Cmp	Al, ';'		; Comment line?
	jnz	Read_Assign_Err
	Call	Skip_Past_EOL
	Jmps	Parse_Line
;
Number_Found:
	Push	Ss
	Push	Bx
	Push	Ss
	Push	Si
	Push	Ss
	Push	Di
	Call	Look_For_File
	jc	Read_Assign_Err
	Call	Skip_Past_EOL
	Dec	Ah
Read_Assign_Err:
	Dec	Ah
Read_Assign_End:
	Or	Ah, Ah
	Retf
;
Skip_Past_EOL:

	Cmp	Al,01ah			; EOF?
	Je	SPE_Ret		; Jump if end

	Cmp	Al,00ah			; Lf?
	Je	Skip_EOL		; Skip EOL if so
	Cmp	Al,00dh			; Cr?
	Je	Skip_EOL		; Skip EOL if so

	Push	Ss
	Push	Bx
	Push	Ss
	Push	Si
	Call	GetACh			; Skip a character

	Jmps	Skip_Past_EOL	;  & go try again for EOL

Skip_EOL:
	Push	Ss
	Push	Bx
	Push	Ss
	Push	Si
	Call	GetACh			; Skip an EOL character

	Cmp	Al,13			; Skip again if Cr
	Je	Skip_EOL
	Cmp	Al,10			; Skip again if Lf
	Je	Skip_EOL

SPE_Ret:
	Ret				; Return; Al has first char past EOL

ShowCsIpString:
	Push	Cs			; Ds must point to code base
	Pop	Ds			;  for Bdos
	Call	PoopCSIP
	Mov	Cl, 9
	Push	Cs
	Call	GSX_Bdos
	Ret
;
ErrorMesg2	Db	'Illegal function:  $'
ErrorMesg7	Db	'GIOS load error on Id $'
HexString	Db	'h (hex)$'
ErrorMesg1	Db	'GIOS invalid$'
GSX_String	Db	'GSX-86 $'

PoopCsIp:
	Push	Ax
	Push	Bx
	Push	Cx
	Push	Dx
	Push	Ds
	Push	Es

	Mov	Dx,Offset GSX_String	; Point to "GSX-86 "
	Mov	Cl,9			; Bdos "Print String" function
	Push	Cs
	Call	GSX_Bdos
	Mov	Dx,OldCs		; Get Gdos caller's Cs
	Call	PoopDx			; Show it

	Mov	Dl,':'			; Show ':' between Cs & Ip
	Call	ShowChar		;  like "xxxx:yyyy"

	Mov	Dx,OldIp		; Get Gdos caller's Ip
	Call	PoopDx			;  & show it

	Mov	Dl,' '			; Show a space
	Call	ShowChar		;  after Ip
	Pop	Es
	Pop	Ds
	Pop	Dx
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Ret				;  & return

ShowChar:
	Mov	Cl,2			; Bdos "Console Output" function
	Push	Cs			; Far call
	Call	GSX_Bdos		; Show the character
	Ret				;  & return

PoopDx:
	Call	Poop2Hex		; Show high-order digits

Poop2Hex:
	Call	PoopHex			; Show high-order digit

PoopHex:
	Mov	Cl,4			; Rotate left by 4
	Rol	Dx,Cl			;  so digit desired is on right
	Push	Dx			; Save unaffected rotated Dx

	And	Dl,00fh			; Mask to single digit
	Cmp	Dl,9			; a..f?
	Jbe	NotAF			; Jump if not a..f
	Add	Dl,7			; Bump a..f to 11..15

NotAF:	Add	Dl,'0'			; 0..9,a..f -> '0'..'9','A'..'F'

	Call	ShowChar		; Show the hex digit

	Pop	Dx			; Restore rotated value to show

	Ret				; Return with rotated value


; Save caller's Dma Segment Base and Dma Offset in ProcCs and ProcIp

PushDmaAddress:
	Push	Ax			; Save caller's registers
	Push	Bx
	Push	Cx
	Push	Es

	Mov	Cl,034h			; Bdos "Get Dma Base & Offset" function
	Push	Cs			; Far call
	Call	GSX_Bdos		; Get Dma Base and Offset in Es:Bx

	Mov	ProcCs,Es		; Save caller's Dma base
	Mov	ProcIp,Bx		;  & offset

	Pop	Es			; Restore registers
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Ret				;  & return

; Input Cx, Ds, Dx (preserved), output Es, Bx, Ax from Bdos function

GSX_Bdos:
	Push	Cx			; Preserve registers from Bdos
	Push	Dx
	Push	Bp
	Push	Si
	Push	Di
	Push	Ds

	Int	Bdos

	Pop	Ds
	Pop	Di
	Pop	Si
	Pop	Bp
	Pop	Dx
	Pop	Cx
	Retf

; Register context should be such that both the driver and the person who
; emitted the Int Gdos think GSX is not there.  In other words, the driver
; should see the Gdos caller's register context and the Gdos caller should,
; upon return, see the driver's context.

Driver:	Push	Bx			; Preserve caller's Ds:Di & Bx
	Push	Di
	Push	Ds

	Push	Bp			; Preserve Bp just prior to driver call

	Push	Cs			; Fake a Callf
	Call	CallDriver		; Invoke the driver

	Push	Bp			; Driver's Bp at [Bp]
	Mov	Bp,Sp			; Point into stack
	Pushf				; Driver's flags below driver's Bp

	Mov	Bp,Ss:(Word Ptr 2)[Bp]	; Get GSX's Bp

	Mov	OldAx,Ax		; Driver's Ax for Gdos caller

	Pop	OldFlags		; Get driver's flags for Gdos caller

	Pop	OldBp			; Get driver's Bp for Gdos caller

	Mov	OldBx,Bx		; Arrange such that caller sees
	Mov	OldCx,Cx		;  driver's registers
	Mov	OldDx,Dx
	Mov	OldSi,Si
	Mov	OldDi,Di
	Mov	OldDs,Ds
	Mov	OldEs,Es

	Pop	Bp			; Get GSX's Bp again

	Pop	Ds			; Get caller's registers
	Pop	Di
	Pop	Bx
	Ret				; Return to InvokeDriver level


CallDriver:
	Push	Cs:(Word Ptr ProcPtr+2)[Bx]; Segment of driver
	Push	Cs:(Word Ptr ProcPtr)[Bx]; Offset of driver
	Mov	Ax,OldAx		; Set up registers for driver
	Mov	Bx,OldBx
	Mov	Cx,OldCx
	Mov	Dx,OldDx
	Mov	Si,OldSi
	Mov	Di,OldDi
;	Mov	Ds,OldDs
	Mov	Es,OldEs
	Mov	Bp,OldBp
	Retf				; Call driver via Retf


; Ds:Di points to parameter block
; OldPtsInSegment:OldPtsInOffset points to PtsIn array
TransformToRaster:
	Les	Si,Ds:(Dword Ptr 0)[Di]	; Point Es:Si to Contrl array

	Mov	Si,Es:(Word Ptr 2)[Si]	; Contrl(2) vertices in PtsIn
	Shl	Si,1			; Times 4 for x,y pair each
	Jz	TtRQuit			; Quit if no vertices

	Mov	Cx,Si			; Twice # of vertices is # of points

	Shl	Si,1			; 2 bytes each point is 4 each vertex
	Sub	Sp,Si			; Enough room in stack for all vertices

	Mov	Ds:(Word Ptr 8)[Di],Sp	; Replace ptr to PtsIn with
	Mov	Ds:(Word Ptr 10)[Di],Ss	;  ptr to transformed PtsIn

	Push	Word Ptr LayerIndex-2	; Push return address

	Push	Bx			; Save caller's registers
	Push	Di
	Push	Ds

	Push	Bp			; Bp used as temp in loop

	Mov	Di,Ds:(Word Ptr 8)[Di]	; Offset from Ss to transformed PtsIn

	Push	Ss			; Transformed PtsIn is in stack
	Pop	Es

	Lds	Si,Dword Ptr OldPtsInOffset	; Point to caller's PtsIn

	Mov	Bp,Cs:(Word Ptr YPixels)[Bx]; Bp holds YPixels at loop begin
	Mov	Bx,Cs:(Word Ptr XPixels)[Bx]; Bx holds XPixels at loop begin

	Cld				; Work up in address


; In transform loop:
;  Bp and Bx swap number of pixels in X and Y, 0..65534 maximum
;  Ds:Si points to caller's PtsIn
;  Es:Di points into stack at transformed PtsIn
;  Cx contains point count; Ax and Dx used in multiply

TtRLoop:
	Lods	Ds:(Word Ptr 0)[Si]	; Get an X or Y
	Shl	Ax,1			; Transform 32K -> 64K space
	Mul	Bx			; Transform 64K -> raster space
	Mov	Ax,Dx			; High-order result is raster space
	Stos	Es:(Word Ptr 0)[Di]	; Stuff in transformed PtsIn

	Xchg	Bx,Bp			; Swap number of pixels in Y or X to Bx

	Loop	TtRLoop			; Loop until all points transformed

	Pop	Bp			; Restore caller's registers
	Pop	Ds
	Pop	Di
	Pop	Bx

TtRQuit:
	Ret				; Return to caller


; Ds:Di point to parameter block

TransformTo32K:
	Les	Si,Ds:(Dword Ptr 0)[Di]	; Get pointer to Contrl array
	Mov	Cx,Es:(Word Ptr 4)[Si]	; Get output vertex count
	Jcxz	Tt32KQuit		; Quit if nothing to transform

	Push	Bx			; Save caller's registers
	Push	Di
	Push	Ds

	Shl	Cx,1			; Twice # vertices is # points

	Lds	Si,Ds:(Dword Ptr 16)[Di]; Ds:Si <- ptr to PtsOut array

	Mov	Di,Cs:(Word Ptr XPixels)[Bx]; Di <- number of pixels in X
	Mov	Bx,Cs:(Word Ptr YPixels)[Bx]; Bx <- number of pixels in Y

	Cld				; Work up in address

; In transform loop:
;  Ds:Si points to PtsOut array
;  Cx holds point count
;  Di and Bx swap number of pixels in X and Y

Tt32KLoop:
	Lods	Ds:(Word Ptr 0)[Si]	; Get a raster-space X or Y
	Mov	Dx,Ax			; Dividend into Dx
	Lea	Ax,-1[Di]		;  with almost divisor on right
	Add	Ax,Di			; Must be almost twice divisor
	Adc	Dx,0			;  on right
	Cmp	Dx,Di			; Would divide overflow?
	Jae	Tt32KOflow		; Jump if yes -- pin to max
	Div	Di			; Transform raster point to 64K space
	Shr	Ax,1			;  to 32K space

Tt32KStuff:
	Mov	Ds:(Word Ptr (-2))[Si],Ax; Back into PtsOut

	Xchg	Bx,Di			; Di <- number of pixels in Y or X

	Loop	Tt32KLoop		; Loop until all vertices transformed

	Pop	Ds			; Restore caller's registers
	Pop	Di
	Pop	Bx

Tt32KQuit:
	Ret				;  & return

Tt32KOFlow:
	Mov	Ax,32767		; Pin returned 32k point at max
	Js	Tt32KStuff		; Return maximum value
					; [BUG] Should be Jmps
;
Look_For_Number:
	Push	Ds
	Push	Ax
	Push	Cx
	Push	Dx
	Push	Bp
	Mov	Bp, Sp
	Lds	Cx, 10h[Bp]
	Push	Ds
	Push	Cx
	Lds	Cx, 0Ch[Bp]
	Push	Ds
	Push	Cx
	Call	NextNonWhitespace
	Call	IsDigit
	Jc	NParseQuit
	Sub	Dx, Dx		; Start with a zero
; Ascii digit in Al; accumulated number in Dx
NParseLoop:
	Cbw				; 0 to Ah
	Sub	Al,'0'			; Make digit into value
	Mov	Cx,Ax			; New digit in Cx
	Mov	Al,10			; 10 times previous accum
	Mul	Dx			;  plus this digit
	Add	Ax,Cx			;  makes new
	Xchg	Ax,Dx			;  accumulated number
	Call	GetDigit		; Get possible next number byte
	Jnc	NParseLoop
	Mov	2[Bp], Dx		; Save result
	Clc
NParseQuit:
	Mov	6[Bp], Al
	Pop	Bp
	Pop	Dx
	Pop	Cx
	Pop	Ax
	Pop	Ds
	Ret	8
;
GetDigit:
	Lds	Cx, 10h[Bp]
	Push	Ds
	Push	Cx
	Lds	Cx, 0Ch[Bp]
	Push	Ds
	Push	Cx
	Call	GetACh
IsDigit:
	Cmp	Al, '0'
	Jc	NotDigit
	Cmp	Al, '9'+1
	Cmc
NotDigit:
	Ret
;
Look_For_File:
	Push	Ds
	Push	Ax
	Push	Cx
	Push	Si
	Push	Di	
	Push	Es
	Push	Bp
	Mov	Bp, Sp
	Les	Di, 10h[Bp]
	Lds	Si, 18h[Bp]
	Push	Ds
	Push	Si
	Lds	Si, 14h[Bp]
	Push	Ds
	Push	Si
	Call	NextNonWhitespace	
	Call	FUpCase
	Cmp	Al,64			; Potential <drivechar>?  @ is logged
	Jb	FTryFN			; No; try for filename
	Cmp	Al,'P'			; Maybe still potential filename?
	Ja	FTryFN			; Jump if not in range '@'..'P'
	Push	Ax			; Save possible <drivechar>
	Lds	Si, 18h[Bp]
	Push	Ds
	Push	Si
	Lds	Si, 14h[Bp]
	Push	Ds
	Push	Si
	Call	FGetCh			; Get next char
	Cmp	Al,':' 
	Jne	FNDriv			; Jump if drive not specified
					; Drive specified?
	Pop	Ax			; Get <drivechar> back
	Sub	Al,'@'			; 'A' => 1 .. 'P' => 16 drive code
	Mov	Es:[Di],Al		; Stuff drive code
	Lds	Si,18h[Bp]
	Push	Ds
	Push	Si
	Push	Ds
	Push	Si
	Lds	Si,14h[Bp]
	Push	Ds
	Push	Si
	Call	GetACh			; Real GetCh of ':'
	Push	Ds
	Push	Si
	Call	GetACh			; GetCh first filename char
	Call	FUpCase			;  & upcase it
	Push	Ax			; Compensate for following pop
FNDriv:	Pop	Ax			; Restore first filename char
FTryFN:					; Point to first filename char in Fcb
	Inc	Di			; Point Di at location of filename
	Mov	Cx,8			; Name is 8 characters long
	Call	FPName			; Parse and fetch name, if present
	Cmp	Al,'.'			; Extension follows?
	Jne	FAftExt			; Jump if no extension
	Lds	Si,18h[Bp]
	Push	Ds
	Push	Si
	Lds	Si,14h[Bp]
	Push	Ds
	Push	Si
	Call	GetACh			; Skip the '.'
	Call	FUpCase			;  & upcase next character
	Mov	Cl,3			; Extension is 3 chars long
	Call	FPName			; Parse and fetch extension
FAftExt:
	Mov	10[Bp], al
	Cmp	Cl,8
	Cmc
	Pop	Bp
	Pop	Es
	Pop	Di
	Pop	Si
	Pop	Cx
	Pop	Ax
	Pop	Ds
	Retn	0Ch
;
FPName:	Pushf
	Cld
FPNameS:
	call	FNameCh
	Jc	FPNameQ			; Quit if not valid name char
	Jcxz	FPNameN			; Bypass store if field full
	Stos	Es:Byte Ptr[Di]		; Stuff & increment field ptr
	Dec	Cx			; Decrement char count

FPNameN:
	Lds	Si,18h[Bp]
	Push	Ds
	Push	Si
	Lds	Si,14h[Bp]
	Push	Ds
	Push	Si
	Call	GetACh			; Get next char
	Call	FUpCase			;  & upcase it
	Jmps	FPNameS			; Loop until name exhausted
;
FPNameQ:
	Push	Cx			; Save indicator of num chars found
	Push	Ax			; Save next character
	Mov	Al,' '			; Clear the rest of the field
    Rep	Stos	Es:Byte Ptr[Di]		;  to spaces; note Rep is a While
	Pop	Ax			; Restore next character
	Pop	Cx			; Restore num chars found indicator
	Popf
	Ret				;  & return

; Return carry reset if valid name character in Al
FNameCh:
	Push	Di
	Push	Es
	Push	Cs
	Pop	Es
	Cmp	Al,080h			; If char >= 080h then carry is clear,
	Jnb	FNamChC			;  so return invalid character
	Mov	Di,Offset FNamChs	; Point to valid character table
FNamChL:
	Scas	(Byte Ptr 0)[Di]	; Compare with bottom of range, bump Di
	Jb	FNamChQ			; Quit if below; not valid
	Scas	(Byte Ptr 0)[Di]	; Compare with top+1 of range, bump Di
	Jnb	FNamChL			; Loop if not in range
FNamChC:
	Cmc				; Complement carry
FNamChQ:
	Pop	Es
	Pop	Di
	Ret
;
FNamChs	db	'!*+,-./:@[\]^', 7Fh, 0FFh


Open_Driver:
	Push	Ds
	Push	Cx
	Push	Si
	Push	Bp	
	Mov	Bp, Sp
	Lds	Si, 10h[bp]	; FCB
	Push	Ds
	Push	Si
	Push	Ds
	Push	Si
	Lds	Si, 0Ch[bp]	; Record buffer
	Push	Ds
	Push	Si
	Mov	Al, ErrorCode4		; Could not open driver
	Call	Open_FCB
	Jc	Open_Driver_End
	Push	Ds
	Push	Si
	Mov	Al, ErrorCode5		; Driver in wrong format
	Call	Read_Record		; Load CMD header
	Jc	Open_Driver_End

	Xor	Bx,Bx			; Start with zero size
	Mov	Cx,8			; Scan 8 buckets
	Mov	Al,ErrorCode6
SizeLoop:
	Test	Byte Ptr 2[Si],00fh	; Bucket present?
	Jz	NextBucket		; Jump if nope

	Test	Word Ptr 5[Si],0ffffh	; Absolute bucket?
	Jnz	Open_Driver_End		; Jump if yes

	Add	Bx,word ptr 7[Si]	; Include this bucket

NextBucket:
	Add	Si,9			; Point to next bucket
	Loop	SizeLoop		;  & loop until all buckets looked at

	Mov	Al, 0			; No error
Open_Driver_End:
	Mov	Sp, Bp
	Pop	Bp
	Pop	Si
	Pop	Cx
	Pop	Ds
	Or	Al, Al
	Retf	8
;
Load_Driver:
	Push	Ds
	Mov	Al, 0
	Push	Ax
	Push	Bx
	Push	Cx
	Push	Dx
	Push	Si
	Push	Di
	Push	Es
	Pushf
	Push	Bp
	Mov	Bp, Sp
	Sub	Sp, 82h			; Record buffer
	Mov	byte ptr -81h[Bp], 1Ah
	Mov	byte ptr -82h[Bp], 80h	; force getrecord on first getchar call
	Lds	Bx, 16h[Bp]		; Pointer to module table entry
	Mov	Si,CS:BasePageSegment[bx] ; Get base address of place to put prog
	Lds	Bx, 1Ah[Bp]		; Point to header record
	Mov	Cx,8			; Look through 8 groups

FindFirstToAllocate:
	Mov	Al, 2[Bx]		; Get Group Format code
	And	Al,00fh			; Only right 4 bits matter
	Cmp	Al,2			; Data bucket?
	Jne	FindFirstLoop		; Jump if nope

	Mov	Word Ptr 5[Bx],Si	; Allocate first DSeg first
	Add	Si,Word Ptr 7[Bx]	;  with length G-min
	Jmps	AllocateMemory		;  then allocate the rest

FindFirstLoop:
	Add	Bx,9		; Bump to next possible group
	Loop	FindFirstToAllocate	;  & loop until 8 groups scanned

AllocateMemory:
	Lds	Bx, 1Ah[Bp]		; Point to beginning of header again
	Mov	Cl,8			; Look through 8 groups

AllocateGroup:
	Cmp	Word Ptr 5[Bx],0	; Already allocated?
	Jne	AllocateLoop		; Jump if yes

	Test	Byte Ptr 2[Bx],00fh	;No group present?
	Jz	AllocateLoop		; Jump if group absent

AllocateIt:
	Mov	Word Ptr 5[Bx],Si	; Allocate memory for group
	Add	Si,Word Ptr 7[Bx]	;  at current base

AllocateLoop:
	Add	Bx,9			; Bump to next group
	Loop	AllocateGroup		; Loop until 8 groups looked at

	Lds	Bx, 1Ah[Bp]		;Point to header yet again
	Mov	Cl,8			; 8 groups to load

LoadGroup:
	Test	Byte Ptr 2[Bx],00fh	; Group present?
	Jz	LoadNextGroup		; Jump if nope

	Push	Cx			; Save group count
	Mov	Cx,Word Ptr 3[Bx]	; Get number of paragraphs in group
	Jcxz	LoadGroupDone		; Jump if empty

	Mov	Es,Word Ptr 5[Bx]	; Point to first place to load

LoadParagraph:
	Sub	Di,Di			; Point Es:Di at byte to load
	Push	Cx			; Save paragraph count
	Mov	Cx,16			; 16 bytes in a paragraph

LoadByte:
	Call	LoadGetCh		; Get a byte
	Cld
	Jnc	StuffByte		; Jump if no unexpected Eof
	Mov	Word Ptr 10h[Bp], 8
	Jmp	RelocateDone

StuffByte:
	Stos	Es:(Byte Ptr 0)[Di]	; Put it here
	Loop	LoadByte		; Loop until paragraph moved
	Mov	Ax,Es			; Bump Es to next paragraph
	Inc	Ax
	Mov	Es,Ax
	Pop	Cx			; Get paragraph count
	Loop	LoadParagraph		; Loop until group loaded
LoadGroupDone:
	Pop	Cx			; Restore group counter
LoadNextGroup:
	Add	Bx,9		; Point to next group in header
	Loop	LoadGroup		;  & loop until all groups loaded
	; Get pointer to layer table entry
	Les	Si,16h[Bp]
	Mov	Es,cs:BasePageSegment[Si] ; Get base page paragraph pointer
	Mov	Di,0			; 48 bytes of base page locate groups
	Mov	Ax,0			;  & start clear
	Mov	Cl,24			; Clear 24 words
    Rep	Stos	Es:(Byte Ptr 0)[Di]	; Clear group locators in base page

	Lds	Bx,1Ah[Bp]		; Point once again to header
	Mov	Cl,8			; Once again scan 8 groups

FixGroupBase:
	Mov	Al,byte ptr 2[bx]	; Get G-Form
	And	Ax,0000fh		;  as word
; Note no provision for MP/M shared code
	Add	Ax,Ax			; *2:  Group present?
	Jz	FixGroupNext		; Jump if nope

	Mov	Di,Ax			; Save *2
	Add	Ax,Ax			; *4
	Add	Di,Ax			; Di has G-Form*6

	Mov	Ax,Word Ptr 5[Bx]	; Get base paragraph
	Mov	Es:(Word Ptr (-3))[Di],Ax;  into base page

	Mov	Ax,Word Ptr 7 [Bx]	; Get number of paragraphs
	Mov	Dx,16			; 16 bytes per paragraph
	Mul	Dx			; To number of bytes
	Sub	Ax,1			; Less 1 for address of last
	Sbb	Dx,0
	Mov	Es:(Word Ptr (-6))[Di],Ax; Low two bytes to base page
	Mov	Es:(Byte Ptr (-4))[Di],Dl; High byte to base page

FixGroupNext:
	Add	Bx,9	
	Loop	FixGroupBase		;  & loop until all groups scanned

	; Point to layer table entry
	Lds	Bx,16h[Bp]
	Mov	Ax,Es:(Word Ptr .3)	; Get code group base
	Mov	Cs:(Word Ptr ProcPtr+2)[Bx],Ax	;  into ProcPtr
	Mov	Cs:(Word Ptr ProcPtr)[Bx],0	; Assume not 8080 model

	Cmp	Es:(Word Ptr .9),0	; DSeg present?
	Jne	FixupReloc		; Jump if yes -- not 8080 model

	Mov	Al,1			; 8080 model:
	Mov	Cs:(Byte Ptr ProcPtr+1)[Bx],Al	; Set ProcPtr offset to 00100h
	Mov	Es:(Byte Ptr .5),Al	; Set M80 byte

FixupReloc:	
	Mov	Byte Ptr -82h[Bp],128	; Force FGetRec for reloc

; Es still points at base page

RelocateLoop:
	Call	LoadGetCh		; Get Location|Target byte, if any
	Jc	RelocateDone		; Quit if Eof
	Or	Al,Al			; Zero means no more relocate poop
	Jz	RelocateDone		; Quit if done relocating

	Mov	Si,Ax			; Location!Target to Si
	And	Si,0000fh		; Mask to Target
	Mov	Di,Si			; Save *1 for *3
	Add	Si,Si			; *2
	Add	Si,Di			; *3
	Add	Si,Si			; *6 index into base page for Target

	And	Ax,000f0h		; Mask Ax to Location
	Shr	Ax,1			; *8
	Shr	Ax,1			; *4
	Mov	Di,Ax			; *4 to Di
	Shr	Ax,1			; *2
	Add	Di,Ax			; *6 index into base page for Location

	Call	LoadGetCh		; Get low byte of segment offset
	Jc	RelocateDone		; Quit if Eof
	Xchg	Al,Ah			; Save low byte in Ah
	Call	LoadGetCh		; Get high byte of segment offset
	Jc	RelocateDone		; Quit if Eof
	Xchg	Al,Ah			; Low to low, high to high

	Add	Ax,Es:(Word Ptr (-3))[Di]	; Add base of segment to reloc
	Mov	Ds,Ax			; Ds <- segbase of word to reloc

	Call	LoadGetCh		; Get byte offset from segbase
	Jc	RelocateDone		; Quit if Eof

	Xchg	Bx,Ax			; Bl is byte offset
	Mov	Bh,0			; Bx is byte offset

	Mov	Ax,Es:(Word Ptr (-3))[Si]	; Get base of Target bucket

	Add	Ds:(Word Ptr 0)[Bx],Ax	; Relocate Location word by Target seg

	Jmps	RelocateLoop		; Loop until done relocating

Relocatedone:
	Mov	Sp, Bp
	Lds	Si,1Eh[Bp]		; Point to Fcb
	Push	Ds
	Push	Si	
	Push	Cs
	Call	Close_File
	Jnc	Relocate_CloseOK
	Mov	Word Ptr 10h[Bp], ErrorCode8
Relocate_CloseOK:	
	Pop	Bp
	Popf
	Pop	Es
	Pop	Di	
	Pop	Si
	Pop	Dx
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Pop	Ds
	Or	Al, Al
	Retn	0Ch
;
LoadGetCh:
	Push	Word Ptr 20h[Bp]	;FCB
	Push	Word Ptr 1Eh[Bp]
	Push	Ss
	Lea	Dx, -82h[BP]		;Record buffer
	Push	Dx
	Call	GetACh
	Ret
;
Open_FCB:
	Push	Ds
	Push	Ax
	Push	Bx
	Push	Cx
	Push	Bp
	Mov	Bp, Sp
	Lds	Bx, 10h[Bp]		; FCB parameter
	Mov	byte ptr 0Ch[Bx], 0	; Clear extent
	Mov	Cl, 00fh		; Bdos "Open File" opcode
	Call	GSX_Bdos_BX
	Mov	byte ptr 20h[Bx], 0	; Zero record counter
	Lds	Bx, 0Ch[Bp]		; Buffer parameter	
	Mov	byte ptr [Bx], 80h	; Read position
	Mov	byte ptr 1[Bx], 1Ah	; EOF, in case read fails
	Rol	Al, 1			; Result of F_OPEN
	Pop	Bp
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Pop	Ds
	Ret	8
;
Read_Record:
	Push	Ds
	Push	Ax
	Push	Bx
	Push	Cx
	Push	Bp
	Mov	Bp, Sp
	Mov	Bx, 0Eh[bp]	; Load address, segment
	Mov	Cl, 33h		; F_DMASEG
	Call	GSX_Bdos_BX
	Mov	Bx, 0Ch[Bp]	; Load address, offset
	Lea	Bx, 2[Bx]	; You could just INC BX twice
	Mov	Cl, 1Ah		; F_DMAOFF
	Call	GSX_Bdos_BX
	Lds	Bx, 10h[bp]	; FCB address
	Mov	Cl, 14h		; F_READ
	Call	GSX_Bdos_BX
	Shr	Al, 1		; Result
	Sbb	Al, Al
	Lds	Bx, 0Ch[bp]	; Record buffer
	Mov	[Bx], Al
	Pop	Bp
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Pop	Ds
	Ret	8	
;
Close_File:
	Push	Ds
	Push	Ax
	Push	Bx
	Push	Cx
	Push	Bp
	Mov	Bp, Sp
	Lds	Bx, 0Eh[bp]	; FCB
	Mov	Cl, 10h		; F_CLOSE
	Call	GSX_Bdos_BX
	Rol	Al, 1
	Pop	Bp
	Pop	Cx
	Pop	Bx
	Pop	Ax
	Pop	Ds
	Retf	4

GSX_Bdos_BX:
	Push	Bx
	Push	Dx
	Push	Ds
	Push	Es
	Xchg	Dx, Bx
	Push	Cs
	Call	GSX_Bdos
	Pop	Es
	Pop	Ds
	Pop	Dx
	Pop	Bx
	Ret

GetACh:	Push	Ds
	Push	Bx
	Push	Bp
	Mov	Bp, Sp
	Lds	Bx, 0Ch[Bp]	;FCB
	Push	Ds
	Push	Bx
	Lds	Bx, 8[Bp]	;Record buffer
	Push	Ds
	Push	Bx
	Call	FGetCh
	Inc	Byte Ptr [Bx]	;Increase record counter
	Pop	Bp
	Pop	Bx
	Pop	Ds
	Ret	8
;
FGetCh:	Push	Ds
	Push	Bx
	Push	Bp
	Mov	Bp, Sp
	Lds	Bx, 8[Bp]
	Cmp	Byte Ptr [Bx], 80h	;Reached end of record?
	Cmc
	Jnc	NotNextRec
	Push	Word Ptr 0Eh[Bp]
	Push	Word Ptr 0Ch[Bp]
	Push	Ds
	Push	Bx
	Call	Read_Record
NotNextRec:
	Pushf
	Add	Bl,[Bx]
	Adc	Bh, 0
	Popf
	Mov	Al, 2[Bx]
	Pop	Bp	
	Pop	Bx
	Pop	Ds
	Ret	8
;
NextNonWhitespace:
	Push	Ds
	Push	Bx
	Push	Bp
	Mov	Bp, Sp
	Pushf
NNW_Loop:
	Cmp	Al, ' '
	Jz	NNW_Whitespace
	Cmp	Al, 8	;TAB
	jnz	NNW_End
NNW_Whitespace:
	Popf
	Lds	Bx, 0Ch[Bp]	;FCB
	Push	Ds
	Push	Bx
	Lds	Bx, 8[Bp]	;Buffer
	Push	Ds
	Push	Bx
	Call	GetACh
	Pushf
	Jmps	NNW_Loop
;
NNW_End:	
	Popf
	Pop	Bp
	Pop	Bx
	Pop	Ds
	Ret	8
;
FUpCase:
	Pushf
	Cmp	Al,'a'			; Lowercase char?
	Jb	UpCaseQ			; Jump if no
	Cmp	Al,'z'			; Lowercase range?
	Jae	UpCaseQ			; Jump if no
	Sub	Al,'a'-'A'		; UpCase the character in Al
UpCaseQ:
	Popf
	Ret				;  & return to caller


; Data area for GSX-86

MAXPROC		Equ	15		; Largest layer number
MAXPROC_Paragraphs Equ	MAXPROC*16	; Largest layer number * 16
MAXFUNC	Equ	7			; Maximum valid Gdos function

		Public	MAXPROC
		Public	HowBound
		Public	ProcPtr
		Public	BasePageSegment
		Public	ModuleSize
		Public	ProcId
		Public	XPixels
		Public	YPixels
		Public	GSX_Size
		Public	Loaded_GSX_Base		
		Public	GSX_Paras		
; Layer Table

HowBound	Rb	1		; How this layer is bound

ProcPtr		Rd	1		; Dword Ptr to proc in layer

BasePageSegment Rw	1		; Segment of Base Page Table for Proc

ModuleSize	Rw	1		; Total paragraphs allocated

ProcId		Rw	1		; Proc or Device Id of proc now bound

XPixels		Rw	1		; Number of pixels in X from driver
YPixels		Rw	1		; Number of pixels in Y from driver

		Rs	1		; Filler to make 16 bytes

		Rs	MAXPROC*16	; Rest of the layers

GSX_Comm_Seg	Equ	BasePageSegment + 10h
GSX_Min_Stack	Equ	ModuleSize + 10h

Loaded_GSX_Base		Dw	0	; Seg of GSX as loaded
GSX_Paras		Dw	0	; Paragraphs in GSX only
GSX_Top			Rw	0

GSX_Size	Equ	((Offset GSX_Top-Offset GSX_Entry_0)+15)/16
GSX_Bytes	Equ	(Offset GSX_Top-Offset GSX_Entry_0)+15
GSX_Comm_Code	CSeg	Para	Public
		Extrn	GSX_Comm	:Far
		Extrn	GSX_Comm_Paragraphs	:Abs

Graphics_Code   CSeg	Para	Public

		Extrn	Graphics	:Far

	End

